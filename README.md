# 简介
Jenkins是一个开源软件项目，是基于Java开发的一种持续集成工具，用于监控持续重复的工作，旨在提供一个开放易用的软件平台，使软件的持续集成变成可能。

# 基于 Kubernetes 部署 Jenkins 集群环境
## 一、准备环境
- Kubernetes 1.20.0
- Jenkins 2.271
- JDK 11.0.9
- Podman 1.6.4
- Docker 20.10.0
- Docker-Compose 1.18.0
- Helm 3.4.2

### 安装helm 3.4.2
```shell
yum install -y podman docker docker-compose
curl -O https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz
tar -zxvf helm-v3.4.2-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
```

### 使用helm创建持久化存储卷
准备NFS存储服务器一台
|    IP地址   |   port   |   服务   |
|-------------|:--------:|:--------:|
|192.168.1.100| 111,2029 |NFS Server|
```shell
# 安装 nfs-client-provisioner
helm install nfs-storage -n default \
--set nfs.server=192.168.1.100,nfs.path=/data/NFS \
--set storageClass.name=nfs-storage,storageClass.reclaimPolicy=Retain \
apphub/nfs-client-provisioner
```
- nfs.server：指定 nfs 服务地址
- nfs.path：指定 nfs 对应目录
- storageClass.name：此配置用于绑定 PVC 和 PV
- storageClass.reclaimPolicy：回收策略，默认是删除
- -n：命名空间

## 二、在K8S中部署Jenkins
### 1、创建命名空间
```
kubectl create ns cicd
```
### 2、下载项目代码
```
git clone https://gitlab.com/snight/jenkins.git /opt/
```
### 3、部署Jenkins
```shell
kubectl create -f /opt/jenkins/jenkins-serviceaccount.yaml
kubectl create -f /opt/jenkins/jenkins-statuefulset.yaml
kubectl create -f /opt/jenkins/jenkins-service.yaml
```
---
# 基于 Docker-Compose 部署 Jenkins 单机环境
## 一、搭建Jenkins
### 1、准备JDK 11环境
```shell
# 创建java目录
mkdir -p /opt/java

# 下载jdk 11.0.9的压缩包
wget https://code.aliyun.com/kar/ojdk11-11.0.9/raw/master/jdk-11.0.9_linux-x64_bin.tar.gz

# 解压jdk压缩到/opt/java目录
tar -zxvf jdk-11.0.9_linux-x64_bin.tar.gz -C /opt/java
```
### 2、创建本地存放数据的目录
```shell
mkdir -p /opt/jenkins/root
mkdir -p /opt/jenkins/home

# 修改home目录访问权限
chown -R 1000:1000 /opt/jenkins/home

# 修改docker.sock权限，否则jenkins容器会由于权限不足，无法执行宿主机的docker命令
chown -R 1000:0 /run/docker.sock
```
由于创建 /opt/jenkins/home 目录的拥有者是当前的root用户，而容器中jenkins用户的uid为1000，所以一定要修改权限，否则在启动jenkins容器时会失败，使用 `docker logs -f jenkins` 命令会打印下面的错误信息：

>touch: cannot touch ‘/var/jenkins_home/copy_reference_file.log’: Permission denied
Can not write to /var/jenkins_home/copy_reference_file.log. Wrong volume permissions?

### 3.创建docker-compose.yml文件
```yaml
cat > /opt/jenkins/docker-compose.yaml << EOF
version: '3.3'
services:
  jenkins:
    restart: always
    image: jenkins/jenkins:2.235.2-centos7
    container_name: jenkins
    volumes:
      - /opt/jenkins/home:/var/jenkins_home
      - /opt/jenkins/root:/root/.jenkins
      - /opt/java/jdk-11.0.9:/usr/local/jdk
      - /usr/bin/docker:/usr/bin/docker
      - /run/docker.sock:/run/docker.sock
    environment:
      - TZ=Asia/Shanghai
    ports:
      - '80:8080'
      - '50000:50000'
EOF
```
### 4、运行Jenkins容器
```shell
cd /opt/jenkins
docker-compose up -d
```
